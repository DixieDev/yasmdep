use std::fs::File;
use std::io::{BufRead, BufReader, Result};
use std::path::Path;
use clap::{Arg, App};

fn get_dependencies(filename: &str, parent_path: Option<&Path>) -> Result<Vec<String>> {
    let path = Path::new(filename);
    let file = match File::open(&path) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("Error opening {}: {}", filename, e);
            return Err(e);
        },
    };

    let mut includes = Vec::<String>::new();
    
    for line in BufReader::new(file).lines() {
        let mut line = line?;
        if let Some(i) = line.find(';') {
            line = line[0..i].to_string();
        }
        if let Some(i) = line.find("%include") {
            line = line[i+8..].to_string();
            if let Some(j) = line.find("\"") {
                line = line[j+1..].to_string();
                if let Some(k) = line.find("\"") {
                    match parent_path {
                        Some(parent_path) => {
                            let parent_path_str = parent_path.to_str().unwrap_or("");
                            let include_path = format!("{}/{}", parent_path_str, &line[..k]);
                            includes.push(include_path);
                        }
                        None => includes.push(line[..k].to_string())
                    }
                }
            }
        }
    }

    let mut all_nested_includes = Vec::<String>::new();
    for include in includes.iter() {
        let include_path = Path::new(include);
        let include_dir = include_path.parent();
        let mut nested_includes = get_dependencies(&include, include_dir)
                .expect(&format!("Could not find file {}", include));
        all_nested_includes.append(&mut nested_includes)
    }
    includes.append(&mut all_nested_includes);

    Ok(includes)
}

fn main() -> Result<()> { 
  let matches = App::new("YASM Dependency Helper")
      .version("1.0")
      .author("Anthony Arian <anthonyarian96@gmail.com>")
      .about("Helps automate YASM builds by listing include dependencies in various formats")
      .arg(Arg::with_name("file")
          .short("f")
          .long("file")
          .value_name("FILEPATH")
          .help("The YASM file to parse.")
          .required(true)
          .takes_value(true))
      .arg(Arg::with_name("prefix")
          .short("p")
          .long("prefix")
          .value_name("PREFIX")
          .help("Prefixes the output filenames with the provided string")
          .required(false)
          .takes_value(true))
      .arg(Arg::with_name("multiline")
          .short("m")
          .long("multiline")
          .help("If present, output filenames will be separated by newlines instead of spaces")
          .required(false)
          .takes_value(false))
      .get_matches();

  let prefix = matches.value_of("prefix");
  let multiline = matches.is_present("multiline");

  let filename = match matches.value_of("file"){
    Some(f) => f,
    None => return Ok(()),
  };

  let includes = get_dependencies(&filename, None).unwrap();

  for dep in includes {
    if let Some(prefix) = prefix {
      print!("{}", prefix);
    };
    print!("{}", dep);
    if multiline {
      println!("");
    } else {
      print!(" ");
    }
  }
  
  if !multiline {
    println!("");
  }

  Ok(())

}

